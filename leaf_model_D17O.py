def leaf_model_D17O(key1,var1,Cin,d17Oin,d18Oin,An,Aleaf,key2,var2,gm13C,Tleaf,print_table=False):

    """
    Name: leaf_model_D17O
    Author: Gerbrand Koren (Wageningen University)
    Date: April 29th, 2020
    
    ----------------------------------------------------------------------------------
    
    Description:
    
    This model calculates the outgoing D17O signature for a leaf in the cuvette experiment as described by Adnew et al.
    
    List of input variables:

    - Experimental conditions
      
      *airflow      rate of air entering cuvette [L/min]
      *drawdown     CO2 drawdown [ppm]
      
    - Ingoing air characteristics
    
      Cin           ingoing CO2 mixing ratio [ppm]
      d17Oin        ingoing d17O signature [permil VSMOW]
      d18Oin        ingoing d18O signature [permil VSMOW]
    
    - Leaf properties
      
      An            net assimilation rate [umol/(m2s)]
      Aleaf         leaf area [cm2]
      **gs          stomatal conductance [mol/(m2s)]
      **Ci_Ca       CO2-in-intercellular-space to CO2-in-atmosphere ratio [-]
      **Cm_Ca       CO2-in-mesophyll to CO2-in-amtosphere ratio [-]
      gm13C         conductance between intercellular space and chloroplast [mol/(m2s)]
    
    - Leaf water characteristics
    
      Tleaf         leaf water temperature [degree C]
      
    Notes:
    
    *   The user can call the function with the value of aiflow or drawdown for var1. The user is
        required to specify which input parameter is given by using the key1 variable.

    **  The user can call the function with the value of gs, Ci_Ca or Cm_Ca for var2. The user is required
        to specify which input parameter is given by using the key2 variable.
      
    ----------------------------------------------------------------------------------
    
    """
    
    #################################################
    #                                               #
    # INITIALIZATION                                #
    #                                               #
    #################################################
    
    # -- Import statement
    import numpy as np
    
    # -- Output settings
    print_diag_d17Oa = False
    print_diag_d18Oa = False
    
    #################################################
    #                                               #
    # MODEL SETTINGS                                #
    #                                               #
    #################################################
    
    # -- Test key1
    if (key1=='airflow'):
        airflow = var1 # L/min
    elif (key1=='drawdown'):
        drawdown = var1 # ppm
    else:
        print 'Error: key1 not recognized'
        return
    
    # -- Test key2
    if (key2=='gs'):
        gs = var2 # mol/(m2s)
    elif (key2=='Ci_Ca'):
        Ci_Ca = var2 # -
    elif (key2=='Cm_Ca'):
        Cm_Ca = var2 # -
    else:
        print 'Error: key2 not recognized'
        return
    
    #################################################
    #                                               #
    # MODEL PARAMETERS                              #
    #                                               #
    #################################################

    # -- Isotope reference
    R17_VSMOW = 0.0003799 # -
    R18_VSMOW = 0.0020052 # -

    # -- Reference line
    RL = 0.528 # -
    
    # -- Physical constants
    Vm = 24.5 # L/mol
        
    # -- 17O fractionations
    lambda_kinetic = 0.507 # (Young et al., 2002)
    lambda_equilibration = 0.5229 # (Barkan and Luz, 2012)
    a17O_liq = 0.382 # per mil
    a17s = 4.44 # per mil
    
    # -- 18O fractionations
    a18s = 8.8 # per mil
    a18O_diff = 8.8 # permil (Farquhar et al., 1993)
    a18O_liq = 0.8 # permil (Farquhar et al., 1993)
   
    #################################################
    #                                               #
    # CALCULATION                                   #
    #                                               #
    #################################################
    
    # ==== MESOPHYLL SIGNATURE
    
    # -- Water vapor mole fractions
    we = 0.09 # mol/mol
    wa = 0.02 # mol/mol

    # -- Water vapor signatures
    d18Owe = -12.0 # per mil
    d17Owe = -6.416 # per mil
    d18Owa = -7.11 # per mil
    d17Owa = -3.963 # per mil
    
    # -- Transpired water signatures
    d18Otrans = (d18Owa*(1-we)-d18Owe*we/wa*(1-wa))*wa/(wa-we) # per mil
    d17Otrans = (d17Owa*(1-we)-d17Owe*we/wa*(1-wa))*wa/(wa-we) # per mil
   
    # -- Water in intercellular air space
    P = 1 # bar
    RH = 1.0 # -
    wi = RH*611.21*np.exp(17.502*Tleaf/(240.97+Tleaf))/(1e5*P) # mol/mol
    
    # -- Water signature at evaporation site
    e17k = 14.6 # per mil
    e18k = 28.0 # per mil
    lambda_k = 0.529 # -
    e18equ = 2.644-3.206*(1e3/(Tleaf+273.15))+1.534*(1e6/((Tleaf+273.15)**2)) # per mil
    e17equ = 1e3*((e18equ*1e-3+1.0)**lambda_k-1) # per mil
    d17Owes = d17Otrans+e17k+e17equ+wa/wi*(d17Owa-e17k-d17Otrans) # per mil
    d18Owes = d18Otrans+e18k+e18equ+wa/wi*(d18Owa-e18k-d18Otrans) # per mil

    # --  Calculate CO2-H2O equilibration
    lambda_CO2_H2O = 0.5229 # -
    e18CO2 = 17604/(Tleaf+273.15)-17.93 # per mil
    e17CO2 = 1e3*((e18CO2*1e-3+1.0)**lambda_CO2_H2O-1) # per mil
    d17Om = 1e3*((d17Owes*1e-3+1)*(1+e17CO2*1e-3)-1) # per mil
    d18Om = 1e3*((d18Owes*1e-3+1)*(1+e18CO2*1e-3)-1) # per mil
    

    # ==== CO2 MIXING RATIOS
    
    # -- CO2 assimilation
    CO2_assimilation = An*1.0e-6*Aleaf*1.0e-4 # molCO2/s
    
    # -- Test key1
    if (key1=='airflow'):
        
        # -- CO2 fluxes
        CO2_inflow = airflow/60.0/Vm*Cin*1.0e-6 # molCO2/s
        CO2_outflow = CO2_inflow-CO2_assimilation # molCO2/s

        # -- Cuvette condition
        Ca = CO2_outflow/(airflow/60.0)*Vm*1.0e6 # ppm
    
    # -- Test key1
    if (key1=='drawdown'):
        
        # -- Airflow
        airflow = CO2_assimilation*60.0*Vm/(drawdown*1.0e-6) # L/min
        
        # -- Cuvette condition
        Ca = Cin-drawdown # ppm
    
    # -- Mesophyll conductance
    gm18O = 3.0*gm13C # mol/(m2s)

    # -- Test key2
    if (key2=='gs'):

        # -- Intercellular CO2 mixing ratio
        Ci = Ca-An/gs # ppm
        
        # -- Mesophyll CO2 mixing ratio
        Cm = Ci-An/gm18O # ppm

    # -- Test key2
    if (key2=='Ci_Ca'):

        # -- Intercellular CO2 mixing ratio
        Ci = Ci_Ca*Ca # ppm
    
        # -- Mesophyll CO2 mixing ratio
        Cm = Ci-An/gm18O # ppm

        # -- Stomatal conductance
        gs = An/(Ca-Ci) # mol/(m2s)

    # -- Test key2
    if (key2=='Cm_Ca'):
    
        # -- Mesophyll CO2 mixing ratio
        Cm = Cm_Ca*Ca # ppm

        # -- Combined stomatal-mesophyll conductance
        gtot = An/(Ca-Cm) # mol/(m2s)

        # -- Stomatal conductance
        gs = 1.0/(1.0/gtot-1.0/gm18O) # mol/(m2s)
        
        # -- Intercellular CO2 mixing ratio
        Ci = Ca-An/gs # ppm
    
    # ==== C17OO MIXING RATIOS

    # -- Mesophyll C17OO mixing ratio
    R17m = (d17Om/1000.0+1.0)*R17_VSMOW # -
    Cm_17O = 2*R17m*Cm # ppm

    # -- C17OO conductances
    gs_17O = gs*((1.0-a18O_diff/1000.0)**lambda_kinetic) # mol/(m2s)
    gm18O_17O = gm18O*((1.0-a17O_liq/1000.0)) # mol/(m2s)

    # -- Ingoing C17OO mixing ratio
    R17in = (d17Oin/1000.0+1.0)*R17_VSMOW # -
    Cin_17O = 2*R17in*Cin # ppm

    # -- C17OO signature
    d17Oa = d17Oin # permil VSMOW
    max_qq = 200 # -
    relax_qq = 0.5 # -
    for qq in np.arange(max_qq):
 
        # -- Atmospheric C17OO mixing ratio
        R17a = (d17Oa/1000.0+1.0)*R17_VSMOW # -
        Ca_17O = 2*R17a*Ca # ppm
    
        # -- Intercellular d17O signature
        Ci_17O = (Ca_17O*gs_17O+Cm_17O*gm18O_17O)/(gs_17O+gm18O_17O) # ppm
        R17i = 0.5*Ci_17O/Ci # -
        d17Oi = 1000.0*(R17i/R17_VSMOW-1.0) # permil VSMOW
    
        # -- Calculate C17OO fluxes
        inflow_C17OO = airflow/60.0/Vm*Cin_17O*1.0e-6 # molC17OO/s
        leafuptake_C17OO = (Ca_17O-Ci_17O)*gs_17O*1.0e-6*Aleaf*1.0e-4 # molC17OO/s
        outflow_C17OO = inflow_C17OO-leafuptake_C17OO # molC17OO/s
    
        # -- Copy previous d17Oa signature
        d17Oa_prev = d17Oa # permil VSMOW
    
        # -- Calculate new d17Oa signature
        Ca_17O = outflow_C17OO/(airflow/60.0)*Vm*1.0e6 # ppm
        R17a = 0.5*Ca_17O/Ca # -
        d17Oa_diff = 1000.0*(R17a/R17_VSMOW-1.0)-d17Oa # permil VSMOW
        d17Oa = d17Oa+relax_qq*d17Oa_diff # permil VSMOW
    
        # -- Print diagnostics
        if (print_diag_d17Oa):
            print '---------------------------------------'
            print 'd17Oa iteration ', qq+1
            print '---------------------------------------'
            print 'Ca_17O          ', Ca_17O
            print 'Ci_17O          ', Ci_17O
            print 'Cm_17O          ', Cm_17O
            print 'R17a            ', R17a
            print 'R17i            ', R17i
            print 'R17m            ', R17m
            print 'd17Oin          ', d17Oin
            print 'd17Oa           ', d17Oa
            print 'd17Oi           ', d17Oi
            print 'd17Om           ', d17Om
            print '---------------------------------------'
            print
    
    # -- Clear loop index
    del qq
        
    # -- Determine convergence
    conv_d17Oa = np.abs((d17Oa-d17Oa_prev)/d17Oa_prev) # -

    # -- Test convergence
    if (conv_d17Oa > 1.0e-6):
        print 'WARNING: Convergence in outgoing d17O signature less than 1.0e-6'
        print 'conv ', conv_d17Oa
        print
        
    # ==== C18OO MIXING RATIOS

    # -- Mesophyll C18OO mixing ratio
    R18m = (d18Om/1000.0+1.0)*R18_VSMOW # -
    Cm_18O = 2*R18m*Cm # ppm

    # -- C18OO conductances
    gs_18O = gs*(1.0-a18O_diff/1000.0) # mol/(m2s)
    gm18O_18O = gm18O*(1.0-a18O_liq/1000.0) # mol/(m2s)

    # -- Ingoing C18OO mixing ratio
    R18in = (d18Oin/1000.0+1.0)*R18_VSMOW # -
    Cin_18O = 2*R18in*Cin # ppm

    # -- C18OO signature
    d18Oa = d18Oin # permil VSMOW
    max_pp = 200 # -
    relax_pp = 0.5 # -
    for pp in np.arange(max_pp):
 
        # -- Atmospheric C18OO mixing ratio
        R18a = (d18Oa/1000.0+1.0)*R18_VSMOW # -
        Ca_18O = 2*R18a*Ca # ppm
    
        # -- Intercellular d18O signature
        Ci_18O = (Ca_18O*gs_18O+Cm_18O*gm18O_18O)/(gs_18O+gm18O_18O) # ppm
        R18i = 0.5*Ci_18O/Ci # -
        d18Oi = 1000.0*(R18i/R18_VSMOW-1.0) # permil VSMOW
    
        # -- Calculate C18OO fluxes
        inflow_C18OO = airflow/60.0/Vm*Cin_18O*1.0e-6 # molC18OO/s
        leafuptake_C18OO = (Ca_18O-Ci_18O)*gs_18O*1.0e-6*Aleaf*1.0e-4 # molC18OO/s
        outflow_C18OO = inflow_C18OO-leafuptake_C18OO # molC18OO/s
    
        # -- Copy previous d18Oa signature
        d18Oa_prev = d18Oa # permil VSMOW
    
        # -- Calculate new d18Oa signature
        Ca_18O = outflow_C18OO/(airflow/60.0)*Vm*1.0e6 # ppm
        R18a = 0.5*Ca_18O/Ca # -
        d18Oa_diff = 1000.0*(R18a/R18_VSMOW-1.0)-d18Oa # permil VSMOW
        d18Oa = d18Oa+relax_pp*d18Oa_diff # permil VSMOW
    
        # -- Print diagnostics
        if (print_diag_d18Oa):
            print '---------------------------------------'
            print 'd18Oa iteration ', pp+1
            print '---------------------------------------'
            print 'Ca_18O          ', Ca_18O
            print 'Ci_18O          ', Ci_18O
            print 'Cm_18O          ', Cm_18O
            print 'R18a            ', R18a
            print 'R18i            ', R18i
            print 'R18m            ', R18m
            print 'd18Oin          ', d18Oin
            print 'd18Oa           ', d18Oa
            print 'd18Oi           ', d18Oi
            print 'd18Om           ', d18Om
            print '---------------------------------------'
            print
    
    # -- Clear loop index
    del pp
        
    # -- Determine convergence
    conv_d18Oa = np.abs((d18Oa-d18Oa_prev)/d18Oa_prev) # -

    # -- Test convergence
    if (conv_d18Oa > 1.0e-6):
        print 'WARNING: Convergence in outgoing d18O signature less than 1.0e-6'
        print 'conv ', conv_d18Oa
        print
        
    # ==== MESOPHYLL SIGNATURES (Farquhar & Cernusak, 2012)
    
    # -- d18O mesophyll signature estimate
    d18OA = d18Oa-Cin/(Cin-Ca)*(d18Oa-d18Oin) # per mil
    d18Om_est = d18OA*(1-Ci/Cm)*(1+a18O_liq/1000.0)+Ci/Cm*(d18Oi-a18O_liq)+a18O_liq # per mil
    
    # -- d17O mesophyll signature estimate
    d17OA = d17Oa-Cin/(Cin-Ca)*(d17Oa-d17Oin) # per mil
    d17Om_est = d17OA*(1-Ci/Cm)*(1+a17O_liq/1000.0)+Ci/Cm*(d17Oi-a17O_liq)+a17O_liq # per mil
        
    # ==== INTERCELLULAR SIGNATURES
    
    ##########################################################
    #                                                        #
    # Note that we use the approximation Cs = Ca             #
    #                                                        #
    ##########################################################
    
    # -- d17O intercellular signature estimate
    d17Oi_est = d17OA*(1-Ca/Ci)*(1+a17s/1000.0)+Ca/Ci*(d17Oa-a17s)+a17s # per mil
    
    # -- d18O intercellular signature estimate
    d18Oi_est = d18OA*(1-Ca/Ci)*(1+a18s/1000.0)+Ca/Ci*(d18Oa-a18s)+a18s # per mil
    
    # ==== EXCESS-17O

    # -- Calculate e17O in regions
    e17Oin = 1.0e6*(np.log(d17Oin/1000.0+1.0)-RL*np.log(d18Oin/1000.0+1.0)) # per meg
    e17Oa = 1.0e6*(np.log(d17Oa/1000.0+1.0)-RL*np.log(d18Oa/1000.0+1.0)) # per meg
    e17Oi = 1.0e6*(np.log(d17Oi/1000.0+1.0)-RL*np.log(d18Oi/1000.0+1.0)) # per meg
    e17Oi_est = 1.0e6*(np.log(d17Oi_est/1000.0+1.0)-RL*np.log(d18Oi_est/1000.0+1.0)) # per meg
    e17Om = 1.0e6*(np.log(d17Om/1000.0+1.0)-RL*np.log(d18Om/1000.0+1.0)) # per meg
    e17Om_est = 1.0e6*(np.log(d17Om_est/1000.0+1.0)-RL*np.log(d18Om_est/1000.0+1.0)) # per meg
    
    # -- Calculate e17O in regions
    e17Oin_linear = 1.0e6*((d17Oin/1000.0)-RL*(d18Oin/1000.0)) # per meg
    e17Oa_linear = 1.0e6*((d17Oa/1000.0)-RL*(d18Oa/1000.0)) # per meg
    e17Oi_linear = 1.0e6*((d17Oi/1000.0)-RL*(d18Oi/1000.0)) # per meg
    e17Oi_est_linear = 1.0e6*((d17Oi_est/1000.0)-RL*(d18Oi_est/1000.0)) # per meg
    e17Om_linear = 1.0e6*((d17Om/1000.0)-RL*(d18Om/1000.0)) # per meg
    e17Om_est_linear = 1.0e6*((d17Om_est/1000.0)-RL*(d18Om_est/1000.0)) # per meg
    
    # ==== MESOPHYLL CONDUCTANCE
    
    # -- Estimate mesophyll conductance
    gm18O_estd17O = An/Ci*(d17OA*(1-a17O_liq/1000.0)+a17O_liq-d17Om)/(d17Oi-d17Om) # mol/(m2s)
    gm18O_estd18O = An/Ci*(d18OA*(1-a18O_liq/1000.0)+a18O_liq-d18Om)/(d18Oi-d18Om) # mol/(m2s)
    e17Ow = a17O_liq-RL*a18O_liq # mol/(m2s)
    e17OA = d17OA*(1-a17O_liq/1000.0)-RL*(d18OA*(1-a18O_liq/1000.0)) # mol/(m2s)
    gm18O_este17O = An/Ci*(e17Om_linear/1000.0-e17OA-e17Ow)/(e17Om_linear/1000.0-e17Oi_linear/1000.0) # mol/(m2s)
    
    # ==== TESTS
    
    # -- Test concentrations
    if (Cin<0 or Ca<0 or Ci<0 or Cm<0 or Cin_17O<0 or Ca_17O<0 or Ci_17O<0 or Cm_17O<0 or Cin_18O<0 or Ca_18O<0 or Ci_18O<0 or Cm_18O<0):
        print 'WARNING: Some concentrations reached a non-physical (negative) value'
        print
        
    # -- Test conductances
    if (gs<0 or gm18O<0 or gs_17O<0 or gm18O_17O<0 or gs_18O<0 or gm18O_18O<0):
        print 'WARNING: Some conductances reached a non-physical (negative) value'
        print
    
    #################################################
    #                                               #
    # RESULTS                                       #
    #                                               #
    #################################################
    
    # -- Test print settings
    if print_table:

        # -- Print parameters
        print '================================================================'
        print 'CO2 mixing ratios'
        print '  ingoing         Cin     %8.1f   ppm' %(Cin)
        print '  atmosphere      Ca      %8.1f   ppm' %(Ca)
        print '  intercellular   Ci      %8.1f   ppm' %(Ci)
        print '  mesophyll       Cm      %8.1f   ppm' %(Cm)
        print
        print 'd17O signatures'
        print '  ingoing         d17Oin  %8.1f   permil VSMOW' %(d17Oin)
        print '  atmosphere      d17Oa   %8.1f   permil VSMOW' %(d17Oa)
        print '  intercellular   d17Oi   %8.1f   permil VSMOW' %(d17Oi)
        print '  mesophyll       d17Om   %8.1f   permil VSMOW' %(d17Om)
        print
        print 'd18O signatures'
        print '  ingoing         d18Oin  %8.1f   permil VSMOW' %(d18Oin)
        print '  atmosphere      d18Oa   %8.1f   permil VSMOW' %(d18Oa)
        print '  intercellular   d18Oi   %8.1f   permil VSMOW' %(d18Oi)
        print '  mesophyll       d18Om   %8.1f   permil VSMOW' %(d18Om)
        print
        print 'e17O signature'
        print '  ingoing         e17Oin  %8.2f   per meg' %(e17Oin)
        print '  atmosphere      e17Oa   %8.2f   per meg' %(e17Oa)
        print '  intercellular   e17Oi   %8.2f   per meg' %(e17Oi)
        print '  mesophyll       e17Om   %8.2f   per meg' %(e17Om)
        print
        print 'e17O signature [linear]'
        print '  ingoing         e17Oin  %8.2f   per meg' %(e17Oin_linear)
        print '  atmosphere      e17Oa   %8.2f   per meg' %(e17Oa_linear)
        print '  intercellular   e17Oi   %8.2f   per meg' %(e17Oi_linear)
        print '  mesophyll       e17Om   %8.2f   per meg' %(e17Om_linear)
        print '================================================================'
        print

    return Ca, Ci, Cm, d17Oa, d18Oa, e17Oa_linear, d17Oi, d18Oi, e17Oi_linear, d17Oi_est, d18Oi_est, e17Oi_est_linear, d17Om, d18Om, e17Om_linear, d17Om_est, d18Om_est, e17Om_est_linear, gm18O_estd17O, gm18O_estd18O, gm18O_este17O