# Leaf conductance model for ∆¹⁷O in CO₂

#### Description

The leaf conductance model calculates steady-state isotopic fractionation in δ¹³C, δ¹⁸O and ∆¹⁷O for a given assimilation rate and set of resistances. A detailed model description is currently in preparation.

#### Publications

The leaf conductance model was used in the following publication:

* Adnew, G. A., Pons, T. L., Koren, G., Peters, W., and Röckmann, T.: Leaf-scale quantification of the effect of photosynthetic gas exchange on Δ¹⁷O of atmospheric CO₂, *Biogeosciences* 17, 3903–3922, [https://doi.org/10.5194/bg-17-3903-2020](https://doi.org/10.5194/bg-17-3903-2020), (2020).

#### Model use

The model is freely available for use. Limited support is available on request from g.b.koren@uu.nl. The model can be cited as

> Koren, G., Adnew, G. A., Röckmann, T., and Peters, W.: Leaf conductance model for ∆¹⁷O in CO₂, [https://git.wur.nl/leaf_model](https://git.wur.nl/leaf_model), (2020)
